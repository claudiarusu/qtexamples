#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qmqtt/qmqtt.h"
#include <QJsonDocument>
#include <QJsonObject>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void convertJson(QMQTT::Message message);
    struct Value
    {
        qreal x;
        qreal y;
        qreal z;
    };

    struct Jsonstruct
    {
        QString DeviceId;
        QString SensActId;
        QString FeatureId;
        Value value;
        QString Timestamp;
        QString StatusCode;
        QString StatusMessage;
    };

private slots:
    void on_connected();
    void on_subscribed(QString topic);
    void on_received(QMQTT::Message message);
    void on_disconnected();
    void on_unsubscribed(QString topic);

private:
    Ui::MainWindow *ui;
    QMQTT::Client *client;
    quint16 port;
    QString topic;
    QString stringMessage;

};

#endif // MAINWINDOW_H
