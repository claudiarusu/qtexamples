#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QHostAddress host("172.16.200.168");
    port = 1883;
    topic = "/#";

    client = new QMQTT::Client(host, port, NULL);
    client->connectToHost();

    connect(client, &QMQTT::Client::connected, this, &MainWindow::on_connected);
    connect(client, &QMQTT::Client::disconnected, this, &MainWindow::on_disconnected);
    connect(client, &QMQTT::Client::subscribed, this, &MainWindow::on_subscribed);
    connect(client, &QMQTT::Client::unsubscribed, this, &MainWindow::on_unsubscribed);
    connect(client, &QMQTT::Client::received, this, &MainWindow::on_received);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_connected()
{
    qInfo() << "conencted";
    client->subscribe("/device/cc3220S-0984/sensor/ccs811/measurement/co2", 0);
}
void MainWindow::on_disconnected()
{
    qInfo() << "disconencted";
}
void MainWindow::on_unsubscribed(QString topic)
{
    qInfo() << "unsubscribed from: " << topic;
}
void MainWindow::on_subscribed(QString topic)
{
    qInfo() << "subscribed at: " << topic;
}
void MainWindow::on_received(QMQTT::Message message)
{
    stringMessage = QString::fromUtf8(message.payload());
    qInfo() << stringMessage;

    //convertJson(message);
}
void MainWindow::convertJson(QMQTT::Message message)
{
    QByteArray messageToByteArray = message.payload();
    QJsonDocument jsonDoc(QJsonDocument::fromJson(messageToByteArray));
    QJsonObject json = jsonDoc.object();
    QJsonValue jvalue = json.value("Value");
    QJsonObject valueobj = jvalue.toObject();


    Jsonstruct *jsonstruct = new Jsonstruct;
    jsonstruct->DeviceId = json["DeviceId"].toString();
    jsonstruct->SensActId = json["SensActId"].toString();
    jsonstruct->FeatureId = json["FeatureId"].toString();
    jsonstruct->Timestamp = json["Timestamp"].toString();
    jsonstruct->StatusCode = json["StatusCode"].toString();
    jsonstruct->StatusMessage = json["StatusMessage"].toString();
    jsonstruct->value.x = valueobj.value("x").toDouble();

    /*qInfo() << jsonstruct->DeviceId;
    qInfo() << jsonstruct->SensActId;
    qInfo() << jsonstruct->FeatureId;
    qInfo() << jsonstruct->Timestamp;
    qInfo() << jsonstruct->StatusCode;
    qInfo() << jsonstruct->StatusMessage;*/

    //qInfo() << jsonstruct->value;

}

















