#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    manager = new QNetworkAccessManager(this);

    connect(manager, &QNetworkAccessManager::finished, this, &MainWindow::onResult);
    manager->get(QNetworkRequest(QUrl("http://172.16.200.168:8081/get-all-devices")));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete manager;
}
void MainWindow::onResult(QNetworkReply *reply)
{
    if(reply->error())
    {
        qInfo() << "ERROR!";
        qInfo() << reply->errorString();
    }
    else
    {
        QByteArray bytesArray = reply->readAll();
        QJsonDocument document = QJsonDocument::fromJson(bytesArray);
        DeviceStruct device[4];

        if(document.isArray())
        {
            QJsonArray documentArray = document.array();

            for(int i = 0; i< documentArray.count(); i++)
            {
                QJsonObject object = documentArray.at(i).toObject();

                device[i].id = object.value("id").toInt();
                device[i].DeviceId = object.value("DeviceId").toInt();
                device[i].SensActId = object.value("SensActId").toInt();
                device[i].FeatureId = object.value("FeatureId").toInt();
                device[i].Topic = object.value("Topic").toString();
                device[i].DataType = object.value("DataType").toString();
                device[i].MeasurementUnit = object.value("MeasurementUnit").toString();
                device[i].Label = object.value("Label").toString();
                device[i].UpperBound = object.value("UpperBound").toInt();
                device[i].LowerBound = object.value("LowerBound").toInt();
                device[i].Location = object.value("Location").toString();
            }
        }
        else
        {
            qInfo() << "Document is not an array";
        }
    }
}






