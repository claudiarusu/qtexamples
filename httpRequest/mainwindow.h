#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    struct DeviceStruct
    {
        qint8 id;
        qint64 DeviceId;
        qint64 SensActId;
        qint64 FeatureId;
        QString Topic;
        QString DataType;
        QString MeasurementUnit;
        QString Label;
        qint16 UpperBound;
        qint16 LowerBound;
        QString Location;
    };

public slots:
    void onResult(QNetworkReply *reply);

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
};

#endif // MAINWINDOW_H
