#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qmqtt/qmqtt.h"
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QSplineSeries>

using namespace QtCharts;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_buttonConnect_clicked();
    void on_buttonSubscribe_clicked();
    void on_connected();
    void on_subscribed(QString topic);
    void on_received(QMQTT::Message message);
    void on_buttonStop_clicked();

private:
    Ui::MainWindow *ui;
    QMQTT::Client *client;
    QHostAddress host;
    quint16 port;
    QString topic;
    QString stringMessage;
    QSplineSeries *series;
    QChart *chart;
    QValueAxis *axis;
    bool stopped;
};

#endif // MAINWINDOW_H
