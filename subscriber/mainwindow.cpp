#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    host = QHostAddress::LocalHost;
    port = 1883;
    topic = "qmqtt/exampletopic";
    stopped = false;
    series = new QSplineSeries();
    chart = new QChart();
    axis = new QValueAxis();

    client = new QMQTT::Client(host, port, NULL);
    client->connectToHost();
    connect(client, &QMQTT::Client::connected, this, &MainWindow::on_connected);
    connect(client, &QMQTT::Client::subscribed, this, &MainWindow::on_subscribed);
    connect(client, &QMQTT::Client::received, this, &MainWindow::on_received);

    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes(); // Preparing the axis
    chart->setAxisX(axis, series);
    axis->setTickCount(5);
    chart->axisX()->setRange(0,20);
    chart->axisY()->setRange(0,20);
    chart->setTitle("Spline chart");
    chart->setAnimationOptions(QChart::AllAnimations);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    setCentralWidget(chartView);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonConnect_clicked()
{
    client->connectToHost();
}

void MainWindow::on_buttonSubscribe_clicked()
{
    client->subscribe(topic, 0);
}
void MainWindow::on_connected()
{
    qInfo() << "conencted";
    client->subscribe(topic, 0);
}
void MainWindow::on_subscribed(QString topic)
{
    qInfo() << "subscribed at: " << topic;
}
void MainWindow::on_received(QMQTT::Message message)
{
    stringMessage = QString::fromUtf8(message.payload());

    QRegExp separator("( )");

    QStringList data = stringMessage.split(separator);
    qInfo() << "Received x: " << data[0];
    qInfo() << "Receivedy : " << data[1];

    if(true != stopped)
    {
        series->append(data[0].toDouble(), data[1].toDouble());
    }
    qreal x = chart->plotArea().width() / axis->tickCount();
    if(data[0].toDouble() > 10.0)
    {
        chart->scroll(x, 0);
    }
}

void MainWindow::on_buttonStop_clicked()
{
    stopped = true;
}
